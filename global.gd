extends Node

var dialog = {}
var has_grimoire = false
var is_break_win_condition = false
var is_win_condition = false
var light_condition_true1 = false
var light_condition_true2 = false
var light_condition_true3 = false
var light_condition_false1 = false

func is_win():
	return is_win_condition and !is_break_win_condition
	
func is_destructible1():
	return light_condition_true1 and light_condition_true2 and light_condition_true3 and light_condition_false1

func reset():
	has_grimoire = false
	is_break_win_condition = false
	is_win_condition = false
	light_condition_true1 = false
	light_condition_true2 = false
	light_condition_true3 = false
	light_condition_false1 = false
