extends Area2D

var scenario1_is_done = false
var scenario2_is_done = false


# Declare member variables here. Examples:
# var a = 2
# var b = "text"


# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass


func _on_Scenario1Area2D_body_shape_entered(body_id, body, body_shape, area_shape):
	if(body.name=="Emiria" and !scenario1_is_done):
		var emiria = get_node("../Emiria")
		emiria.set_physics_process(false)
		emiria.play_animation("Idle_E")
		get_node("../WalkingSoundPlayer").stop()
		yield(get_tree().create_timer(1.0), "timeout")
		emiria.get_node("Camera2D").current = false
		var scenario1_camera = get_node("../Obstacle1/centerfence/Camera2D")
		scenario1_camera.current = true
		scenario1_camera.zoom = Vector2(0.5, 0.5)
		yield(get_tree().create_timer(1.0), "timeout")
		var GUI = get_node("../GUI")
		var dialog = load("res://addons/dialogs/Dialog.tscn").instance()
		if(global.has_grimoire):
			dialog.external_file = "res://Dialogs/scenario1_with_grimoire.json"
		else:
			dialog.external_file = "res://Dialogs/scenario1_without_grimoire.json"
		GUI.add_child(dialog)
		scenario1_is_done = true


func _on_Scenario1Area2D2_body_shape_entered(body_id, body, body_shape, area_shape):
	if(body.name=="Emiria" and !scenario2_is_done):
		var emiria = get_node("../Emiria")
		emiria.set_physics_process(false)
		emiria.play_animation("Idle_N")
		get_node("../WalkingSoundPlayer").stop()
		yield(get_tree().create_timer(1.0), "timeout")
		var GUI = get_node("../GUI")
		var dialog = load("res://addons/dialogs/Dialog.tscn").instance()
		dialog.external_file = "res://Dialogs/scenario2.json"
		GUI.add_child(dialog)
		scenario2_is_done = true
