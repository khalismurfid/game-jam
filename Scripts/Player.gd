extends KinematicBody2D

var curHp : int = 10
var maxHp : int = 10
var moveSpeed : int = 250
var damage : int = 1
 
var gold : int = 0
 
var curLevel : int = 0
var curXp : int = 0
var xpToNextLevel : int = 50
var xpToLevelIncreaseRate : float = 1.2
 
var interactDist : int = 70
 
var vel = Vector2()
var facingDir = Vector2(0, 1)
 
onready var rayCast = $RayCast2D
onready var anim = $AnimatedSprite
onready var light = get_node("Light2D")
onready var hand = $Hand
onready var GUI = get_node("../GUI Magic Command/MagicCommand")
onready var spellLineEdit = GUI.get_node("Panel Container/VBoxContainer/LineEdit")

var light_offset = 20

func _physics_process (delta):
 
	vel = Vector2()
 
	# inputs
	if Input.is_action_pressed("move_up"):
		vel.y -= 1
		facingDir = Vector2(0, -1)
	if Input.is_action_pressed("move_down"):
		vel.y += 1
		facingDir = Vector2(0, 1)
	if Input.is_action_pressed("move_left"):
		vel.x -= 1
		facingDir = Vector2(-1, 0)
	if Input.is_action_pressed("move_right"):
		vel.x += 1
		facingDir = Vector2(1, 0)
	if Input.is_action_just_pressed("shoot"):
		pass
	if Input.is_action_just_pressed("magic_command"):
		if GUI.is_visible():
			process_spell(spellLineEdit.get_text())
			GUI.hide()
		else:
			GUI.show()
			spellLineEdit.clear()
			spellLineEdit.grab_focus()
	# normalize the velocity to prevent faster diagonal movement
	vel = vel.normalized()
 
	# move the player
	move_and_slide(vel * moveSpeed, Vector2.ZERO)
	manage_animations_positions()

func manage_animations_positions ():
	hand.position.x = 0
	hand.position.y = 0
	if vel.x > 0:
		hand.position.x = light_offset
		play_animation("MoveRight")
	elif vel.x < 0:
		hand.position.x = 0-light_offset
		play_animation("MoveLeft")
	elif vel.y < 0:
		hand.position.y = 0-light_offset
		play_animation("MoveUp")
	elif vel.y > 0:
		hand.position.y = light_offset
		play_animation("MoveDown")
	elif facingDir.x == 1:
		hand.position.x = light_offset
		play_animation("IdleRight")
	elif facingDir.x == -1:
		hand.position.x = 0-light_offset
		play_animation("IdleLeft")
	elif facingDir.y == -1:
		hand.position.y = 0-light_offset
		play_animation("IdleUp")
	elif facingDir.y == 1:
		hand.position.y = light_offset
		play_animation("IdleDown")
		

func play_animation (anim_name):
	if anim.animation != anim_name:
		anim.play(anim_name)

func process_spell(spell):
	spell = spell.to_lower()
	if(spell == "lighton"):
		hand.get_node("LightSpell").show()
	if(spell == "lightoff"):
		hand.get_node("LightSpell").hide()
	elif(spell == "fireball"):
		var fireball = preload("res://Scenes/Fireball.tscn").instance()
		fireball.setDirection(facingDir)
		owner.add_child(fireball)
		fireball.transform = $Hand.global_transform
	elif(spell == "iceshard"):
		var iceshard = preload("res://Scenes/IceShard.tscn").instance()
		iceshard.setDirection(facingDir)
		owner.add_child(iceshard)
		iceshard.transform = $Hand.global_transform
