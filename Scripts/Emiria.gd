extends KinematicBody2D

var anim_direction = "S"

var moveSpeed  = 150
var isMoving = true
var stopMoving = false

var vel = Vector2()
var facingDir = Vector2(0, 1)
var playing = true

 
onready var anim = $AnimationPlayer
onready var hand = $Hand
onready var current_scene = get_tree().get_current_scene()
onready var GUI = current_scene.get_node("GUI Magic Command/MagicCommand")
onready var spellLineEdit = get_node("../GUI Magic Command/MagicCommand/Panel Container/VBoxContainer/LineEdit")

var light_offset = 20

func _physics_process (delta):
	vel = Vector2()
	moveSpeed = 150
	# inputs
	if(playing):
		if(!stopMoving) :
			if Input.is_action_pressed("run"):
				moveSpeed = 250
			if Input.is_action_pressed("move_up"):
				isMoving = true
				vel.y -= 1
				facingDir = Vector2(0, -1)
			if Input.is_action_pressed("move_down"):
				isMoving = true
				vel.y += 1
				facingDir = Vector2(0, 1)
			if Input.is_action_pressed("move_left"):
				isMoving = true
				vel.x -= 1
				facingDir = Vector2(-1, 0)
			if Input.is_action_pressed("move_right"):
				isMoving = true
				vel.x += 1
				facingDir = Vector2(1, 0)
		if Input.is_action_just_pressed("magic_command"):
			if(global.has_grimoire):
				if GUI.is_visible(): 
					stopMoving = false
					process_spell(spellLineEdit.get_text())
					GUI.hide()
				else:
					GUI.show()
					stopMoving = true
					spellLineEdit.clear()
					spellLineEdit.grab_focus()
		# normalize the velocity to prevent faster diagonal movement
		vel = vel.normalized()
	 
		# move the player
		move_and_slide(vel * moveSpeed, Vector2.ZERO)
		manage_animations_positions()

func manage_animations_positions ():
	hand.position.x = 0
	hand.position.y = 0
	manage_sound()
	if vel.x > 0:
		hand.position.x = light_offset
		anim_direction = "E"
		play_animation("Walk_E")
	elif vel.x < 0:
		hand.position.x = 0-light_offset
		anim_direction = "W"
		play_animation("Walk_W")
	elif vel.y < 0:
		hand.position.y = 0-light_offset
		anim_direction = "N"
		play_animation("Walk_N")
	elif vel.y > 0:
		hand.position.y = light_offset
		anim_direction = "S"
		play_animation("Walk_S")
	elif facingDir.x == 1:
		if isMoving == true:
			hand.position.x = light_offset
			anim_direction = "E"
			play_animation("Idle_E")
			isMoving = false
	elif facingDir.x == -1:
		if isMoving == true:
			hand.position.x = 0-light_offset
			anim_direction = "W"
			play_animation("Idle_W")
		isMoving = false
	elif facingDir.y == -1:
		if isMoving == true:
			hand.position.y = 0-light_offset
			anim_direction = "N"
			play_animation("Idle_N")
			isMoving = false
	elif facingDir.y == 1:
		if isMoving == true:
			hand.position.y = light_offset
			anim_direction = "S"
			play_animation("Idle_S")
			isMoving = false
		

func manage_sound():
	var player = current_scene.get_node("WalkingSoundPlayer")
	if vel.x != 0 or vel.y !=0:
		if !player.is_playing():
			player.play()
	else:
		player.stop()
func play_animation (anim_name):
	anim.play(anim_name)

func process_spell(spell):
	spell = spell.to_lower()
	playing=false
	anim.play("Spell_"+anim_direction)
	yield(anim, "animation_finished")
	playing=true
	isMoving = true
	if(spell == "light on"):
		hand.get_node("LightSpell").show()
	if(spell == "light off"):
		hand.get_node("LightSpell").hide()
	elif(spell == "fireball"):
		var fireball = preload("res://Scenes/Fireball.tscn").instance()
		fireball.setDirection(facingDir)
		owner.add_child(fireball)
		fireball.transform = $Hand.global_transform
	elif(spell == "ice shard"):
		var iceshard = preload("res://Scenes/IceShard.tscn").instance()
		iceshard.setDirection(facingDir)
		owner.add_child(iceshard)
		iceshard.transform = $Hand.global_transform
	elif(spell == "orb"):
		var orb = preload("res://Scenes/Orb.tscn")
		orb = orb.instance()
		orb.set_position(self.get_position())
		get_node("..").add_child(orb)
