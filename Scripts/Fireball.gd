extends Area2D
var speed = 750
var facingDir = Vector2()
var physicsOn = true

onready var anim = get_node("AnimatedSprite")

func setDirection(facingDir):
	self.facingDir = facingDir
	get_node("AnimatedSprite").rotate(0-facingDir.angle())
	
func _physics_process(delta):
	if physicsOn:
		if facingDir.x == 1:
			position += transform.x * speed * delta
		elif facingDir.x == -1:
			position -= transform.x * speed * delta
		elif facingDir.y == -1:
			position -= transform.y * speed * delta
		elif facingDir.y == 1:
			position += transform.y * speed * delta

func _on_Fireball_body_entered(body):
	if !body.is_in_group("player"):
		var player =  get_tree().get_current_scene().get_node("FireballSoundPlayer")
		self.add_child(player)
		player.stream = load("res://Sounds/explosion.wav")
		player.play()
		$AnimatedSprite/Light2D.energy = 1.2
		physicsOn = false
		if(body.is_in_group("desctructible")):
			body.queue_free()
		if(body.is_in_group('desctructible_condition1')):
			if(global.is_destructible1()):
				global.is_win_condition = true
				body.queue_free()
		anim.play("Collide")
		yield(anim, "animation_finished")
		queue_free()
	


func play_animation (anim_name):
	if anim.animation != anim_name:
		anim.play(anim_name)


func _on_Fireball_area_shape_entered(area_id, area, area_shape, self_shape):
	if area.is_in_group('light_source'):
		if area.is_in_group('light_condition_true1'):
			global.light_condition_true1 = true
		if area.is_in_group('light_condition_true2'):
			global.light_condition_true2 = true
		if area.is_in_group('light_condition_true3'):
			global.light_condition_true3 = true
		area.get_node('fire').show()
		
