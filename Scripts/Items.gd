extends Node

export(int) var king_book_id

var interactive = false
var interactive_king_book = false
var interactive_king_book2 = false
var interactive_king_book3 = false
var interactive_king_book4 = false
var interactive_king_book5 = false
var interactive_king_book6 = false
var interactive_king_book7 = false
var interactive_king_book8 = false
var interactive_king_book9 = false
var interactive_king_book10 = false
var interactive_king_book11 = false
var interactive_king_book12 = false
# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass

func _physics_process(delta):
	if Input.is_action_pressed('interact'):
		if(interactive):
			global.has_grimoire = true
			self.queue_free()
			encounter_dialog("res://Dialogs/pickup_grimoire.json")
		elif(interactive_king_book):
			encounter_dialog("res://Dialogs/king_book_1.json")
		elif(interactive_king_book2):
			encounter_dialog("res://Dialogs/king_book_2.json")
		elif(interactive_king_book3):
			encounter_dialog("res://Dialogs/king_book_3.json")
		elif(interactive_king_book4):
			encounter_dialog("res://Dialogs/king_book_4.json")
		elif(interactive_king_book5):
			encounter_dialog("res://Dialogs/king_book_5.json")
		elif(interactive_king_book6):
			global.is_break_win_condition = true
			encounter_dialog("res://Dialogs/king_book_6.json")
		elif(interactive_king_book7):
			encounter_dialog("res://Dialogs/king_book_7.json")
		elif(interactive_king_book8):
			encounter_dialog("res://Dialogs/king_book_8.json")
		elif(interactive_king_book9):
			encounter_dialog("res://Dialogs/king_book_9.json")
		elif(interactive_king_book10):
			if(global.is_win()):
				encounter_dialog("res://Dialogs/king_book_10_good_ending.json")
			else:
				encounter_dialog("res://Dialogs/king_book_10_bad_ending.json")
		elif(interactive_king_book11):
			encounter_dialog("res://Dialogs/king_book_11.json")
		elif(interactive_king_book12):
			encounter_dialog("res://Dialogs/king_book_12.json")

func encounter_dialog(dialog_path):
	var emiria = get_node("../Emiria")
	if(!emiria.stopMoving):
		emiria.set_physics_process(false)
		global.has_grimoire = true
		var GUI = get_node("../GUI")
		var dialog = load("res://addons/dialogs/Dialog.tscn").instance()
		dialog.external_file = dialog_path
		GUI.add_child(dialog)

func _on_GrimoireArea2D_body_entered(body):
	if(body.name == "Emiria"):
		interactive = true



func _on_GrimoireArea2D_body_exited(body):
	if(body.name == "Emiria"):
		interactive = false


func _on_KingBookArea2D_body_entered(body):
	if(body.name == "Emiria"):
		if(king_book_id == 1):
			interactive_king_book = true
		if(king_book_id == 2):
			interactive_king_book2 = true
		if(king_book_id == 3):
			interactive_king_book3 = true
		if(king_book_id == 4):
			interactive_king_book4 = true
		if(king_book_id == 5):
			interactive_king_book5 = true
		if(king_book_id == 6):
			interactive_king_book6 = true
		if(king_book_id == 7):
			interactive_king_book7 = true
		if(king_book_id == 8):
			interactive_king_book8 = true
		if(king_book_id == 9):
			interactive_king_book9 = true
		if(king_book_id == 10):
			interactive_king_book10 = true
		if(king_book_id == 11):
			interactive_king_book11 = true
		if(king_book_id == 12):
			interactive_king_book12 = true


func _on_KingBookArea2D_body_exited(body):
	if(body.name == "Emiria"):
		if(king_book_id == 1):
			interactive_king_book = false
		if(king_book_id == 2):
			interactive_king_book2 = false
		if(king_book_id == 3):
			interactive_king_book3 = false
		if(king_book_id == 4):
			interactive_king_book4 = false
		if(king_book_id == 5):
			interactive_king_book5 = false
		if(king_book_id == 6):
			interactive_king_book6 = false
		if(king_book_id == 7):
			interactive_king_book7 = false
		if(king_book_id == 8):
			interactive_king_book8 = false
		if(king_book_id == 9):
			interactive_king_book9 = false
		if(king_book_id == 10):
			interactive_king_book10 = false
		if(king_book_id == 11):
			interactive_king_book11 = false
		if(king_book_id == 12):
			interactive_king_book12 = false
