extends Area2D
var speed = 750
var facingDir = Vector2()
var physicsOn = true

onready var anim = get_node("AnimatedSprite")

func setDirection(facingDir):
	self.facingDir = facingDir
	get_node("AnimatedSprite").rotate(0-facingDir.angle())
	
func _physics_process(delta):
	if physicsOn:
		if facingDir.x == 1:
			position += transform.x * speed * delta
		elif facingDir.x == -1:
			position -= transform.x * speed * delta
		elif facingDir.y == -1:
			position -= transform.y * speed * delta
		elif facingDir.y == 1:
			position += transform.y * speed * delta

func play_animation (anim_name):
	if anim.animation != anim_name:
		anim.play(anim_name)


func _on_IceShard_body_entered(body):
	if !body.is_in_group("player"):
		$AnimatedSprite/Light2D.energy = 1
		physicsOn = false
		print(body)
		if(body.is_in_group("frozenable")):
			body.modulate = Color(1, 1, 1)
			body.get_node('CollisionPolygon2D').set_deferred("disabled", true)
		anim.play("Collide")
		yield(anim, "animation_finished")
		queue_free()


func _on_IceShard_area_shape_entered(area_id, area, area_shape, self_shape):
	if area.is_in_group('light_source'):
		if area.is_in_group('light_condition_false1'):
			global.light_condition_false1 = true
		area.get_node('fire').hide()
