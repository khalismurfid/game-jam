extends Node2D
var minX = 500
var minY = 500
onready var pause_menu = get_node("GUI/PauseMenu")
onready var emiria = get_node("Emiria")
func _ready():
	$Fires/Fire26/fire.hide()
	$Fires/Fire27/fire.hide()
	$Fires/Fire28/fire.hide()
	emiria.set_physics_process(false)
	yield(get_tree().create_timer(1.0), "timeout")
	var dialog = load("res://addons/dialogs/Dialog.tscn").instance()
	dialog.external_file = "res://Dialogs/dialog_awake.json"
	$GUI.add_child(dialog)
	
func _physics_process(delta):
	if Input.is_action_just_pressed("pause") and !pause_menu.get_node("GUI/ControlMenu").is_visible():
		if pause_menu.is_visible():
			emiria.playing=true
			$WalkingSoundPlayer.play()
			$BackgroundMusicPlayer.play()
			$RainStreamPlayer.play()
			pause_menu.hide()
		else:
			emiria.playing=false
			$WalkingSoundPlayer.stop()
			$BackgroundMusicPlayer.stop()
			$RainStreamPlayer.stop()
			pause_menu.show()
func resize():
	var currentSize = OS.get_window_size()

	if(currentSize.x < minX):
		OS.set_window_size(Vector2(minX, currentSize.y))

	if(currentSize.y < minY):
		OS.set_window_size(Vector2(currentSize.x, minY))
