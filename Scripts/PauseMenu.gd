extends MarginContainer


# Declare member variables here. Examples:
# var a = 2
# var b = "text"


# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass


func _on_ResumeButton_pressed():
	self.hide()


func _on_ControlsButton_pressed():
	self.hide()
	get_node("GUI/ControlMenu").show()


func _on_ExitButton_pressed():
	get_tree().change_scene("res://Scenes/MainMenu.tscn")
